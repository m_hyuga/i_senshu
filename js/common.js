$(function(){
	//共通部分css調整
	$(".contact li:last-child").css({"padding-right":"0px"});
	$("nav .main_navi li:first-child").css({"background":"none"});

	//ページ内リンク
	$('a[href^=#]').click(function(){
		var speed = 500;
		var href= $(this).attr("href");
		var target = $(href == "#" || href == "" ? 'html' : href);
		var position = target.offset().top;
		$("html, body").animate({scrollTop:position}, speed, "swing");
		return false;
	});
});